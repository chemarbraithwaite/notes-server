module.exports = {
    tables: [
      {
        TableName: `Notes`,
        KeySchema: [
          {AttributeName: 'author', KeyType: 'HASH'},
          {AttributeName: 'id', KeyType: 'RANGE'}
        ],
        AttributeDefinitions: [
          {AttributeName: 'author', AttributeType: 'S'},
          {AttributeName: 'id', AttributeType: 'S'}
        ],
        ProvisionedThroughput: {ReadCapacityUnits: 5, WriteCapacityUnits: 5},
      }
    ],
    basePort: 8000,
};