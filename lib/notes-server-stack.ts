import { AuthorizationType, CognitoUserPoolsAuthorizer, LambdaIntegration, RestApi } from '@aws-cdk/aws-apigateway';
import { OAuthScope, UserPool, UserPoolClient, VerificationEmailStyle } from '@aws-cdk/aws-cognito';
import { AttributeType, Table } from '@aws-cdk/aws-dynamodb';
import { NodejsFunction } from '@aws-cdk/aws-lambda-nodejs';
import * as cdk from '@aws-cdk/core';
import { Duration } from '@aws-cdk/core';
import * as path from 'path';
import { domain_prefix } from './config';
const corsOrigins = ['http://localhost:3000'];

const callbackUrls = ["http://localhost:3000"];


export class NotesServerStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const api = new RestApi(this, 'notes-api', {
      restApiName: 'Notes Service',
      description: 'This service serves notes',
      defaultCorsPreflightOptions: {
        allowOrigins: corsOrigins
      }
    });

    const userPool = new UserPool(this, 'NotesUserPool', {
      removalPolicy: cdk.RemovalPolicy.DESTROY,
      standardAttributes: {
        givenName: {
          required: true,
          mutable: true
        },
        familyName: {
          required: true,
          mutable: true
        },
        email: {
          required: true,
          mutable: false
        }
      },
      signInAliases: {
        email: true,
        username: false,
        phone: false
      },
      passwordPolicy: {
        minLength: 8,
        requireUppercase: true,
        requireLowercase: true,
        requireSymbols: true,
        requireDigits: true
      },
      userVerification: {
        emailSubject: 'Thanks for signing up for Notes!',
        smsMessage: 'Thanks for signing up for Notes! Your verification code is {####}',
        emailBody: 'Thanks for signing up for Notes! Your verification code is {####}',
        emailStyle: VerificationEmailStyle.CODE
      },
      signInCaseSensitive: false,
      selfSignUpEnabled: true,
    });

    userPool.addDomain('notes-dev-domain', {
      cognitoDomain: {
        domainPrefix: `${domain_prefix}-dev`
      }
    });

    new UserPoolClient(this, 'native_client', {
      userPool: userPool,
      userPoolClientName: 'native_client',
      oAuth: {
        callbackUrls: callbackUrls,
        logoutUrls: callbackUrls,
        scopes: [OAuthScope.EMAIL, OAuthScope.OPENID, OAuthScope.COGNITO_ADMIN, OAuthScope.PROFILE, OAuthScope.PHONE],
        flows: {
          authorizationCodeGrant: true
        }
      }
    });

    new UserPoolClient(this, 'web_client', {
      userPool: userPool,
      userPoolClientName: 'web_client',
      oAuth: {
        callbackUrls: callbackUrls,
        logoutUrls: callbackUrls,
        scopes: [OAuthScope.EMAIL, OAuthScope.OPENID, OAuthScope.COGNITO_ADMIN, OAuthScope.PROFILE, OAuthScope.PHONE],
        flows: {
          authorizationCodeGrant: true
        }
      }
    });


    const cognitoAuthorizer = new CognitoUserPoolsAuthorizer(this, 'NoteServiceAuthorizer', {
      cognitoUserPools: [userPool]
    });

    const noteTable = new Table(this, 'Notes', {
      tableName: 'Notes',
      partitionKey: { name: 'author', type: AttributeType.STRING },
      sortKey: { name: 'id', type: AttributeType.STRING },
      removalPolicy: cdk.RemovalPolicy.DESTROY
    });

    const addNote = new NodejsFunction(this, 'AddNotes', {
      entry: path.join(__dirname, '../notes_service/add_note/index.ts'),
      handler: 'handler',
      environment: {
        TABLE_NAME: noteTable.tableName,
        CORS_ORIGINS: corsOrigins.toString()
      }
    });
    noteTable.grantWriteData(addNote);

    const getNotes = new NodejsFunction(this, 'GetNotes', {
      entry: path.join(__dirname, '../notes_service/get_notes/index.ts'),
      handler: 'handler',
      timeout: Duration.seconds(10),
      environment: {
        TABLE_NAME: noteTable.tableName,
        CORS_ORIGINS: corsOrigins.toString()
      }
    });
    noteTable.grantReadData(getNotes);

    const getNote = new NodejsFunction(this, 'GetNote', {
      entry: path.join(__dirname, '../notes_service/get_note/index.ts'),
      handler: 'handler',
      environment: {
        TABLE_NAME: noteTable.tableName,
        CORS_ORIGINS: corsOrigins.toString()
      }
    });
    noteTable.grantReadData(getNote);

    const deleteNote = new NodejsFunction(this, 'DeleteNote', {
      entry: path.join(__dirname, '../notes_service/delete_note/index.ts'),
      handler: 'handler',
      environment: {
        TABLE_NAME: noteTable.tableName,
        CORS_ORIGINS: corsOrigins.toString()
      }
    });
    noteTable.grantFullAccess(deleteNote);

    const editNote = new NodejsFunction(this, 'EditNote', {
      entry: path.join(__dirname, '../notes_service/edit_note/index.ts'),
      handler: 'handler',
      environment: {
        TABLE_NAME: noteTable.tableName,
        CORS_ORIGINS: corsOrigins.toString()
      }
    });
    noteTable.grantWriteData(editNote);

    const usersResource = api.root.addResource('users');

    const authorResource = usersResource.addResource('{author}', {
      defaultMethodOptions: {
        requestParameters: {
          ['method.request.path.author']: true
        }
      }
    });

    const notesResource = authorResource.addResource('notes');


    notesResource.addMethod('POST', new LambdaIntegration(addNote), {
      authorizationType: AuthorizationType.COGNITO,
      authorizer: cognitoAuthorizer
    });

    notesResource.addMethod('GET', new LambdaIntegration(getNotes), {
      authorizationType: AuthorizationType.COGNITO,
      authorizer: cognitoAuthorizer
    });

    const noteResource = notesResource.addResource('{id}', {
      defaultMethodOptions: {
        requestParameters: {
          ['method.request.path.author']: true,
          ['method.request.path.id']: true
        }
      }
    });

    noteResource.addMethod('GET', new LambdaIntegration(getNote), {
      authorizationType: AuthorizationType.COGNITO,
      authorizer: cognitoAuthorizer
    });
    noteResource.addMethod('PATCH', new LambdaIntegration(editNote), {
      authorizationType: AuthorizationType.COGNITO,
      authorizer: cognitoAuthorizer
    });
    noteResource.addMethod('DELETE', new LambdaIntegration(deleteNote), {
      authorizationType: AuthorizationType.COGNITO,
      authorizer: cognitoAuthorizer,
    });
    

  }
}
