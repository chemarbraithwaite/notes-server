import { DynamoDB } from 'aws-sdk';
import { NoteError } from '../_shared/errors';
import {v4 as uuid4v} from 'uuid';
import { APIGatewayEvent } from 'aws-lambda';
const isTest = process.env.JEST_WORKER_ID;

export class Note {
    id: string;
    author: string;
    created: string;
    title: string;
    content: string;
    color: string;

    constructor(note: any, author: string){
        if(!author || !note.title || !note.content || !note.color){
            throw new NoteError(400, 'Invalid request body');
        }

        const date = new Date().toISOString();
        
        this.id = isTest ? process.env.NOTE_ID! : `${date}${uuid4v()}`
        this.author = author;
        this.content = note.content;
        this.color = note.color;
        this.created = isTest ? process.env.CREATED! : date;
        this.title = note.title;
    }

}
 
export const addNote = async (event: APIGatewayEvent) => {

    const tableName = process.env.TABLE_NAME;

    if (!tableName) {
        throw new NoteError(500, "Environment variable 'Table Name' is undefined");
    }

   validateRequest(event)

    const author = event.pathParameters!.author;
    const noteObject = JSON.parse(event.body!);

    const  note = new Note(noteObject, author!);
    const config = getConfig();

    try {
        const docClient = new DynamoDB.DocumentClient(config);

        const item: DynamoDB.DocumentClient.PutItemInput = {
            TableName: tableName,
            Item: {
                ...note
            }
        }

        await docClient.put(item).promise();
    } catch (error) {
        console.log(error);
        throw new NoteError(500, 'Error saving note');
    }
    
    return note;
}

const validateRequest = (event: APIGatewayEvent) => {
    if(!event.pathParameters || !event.pathParameters.author){
        throw new NoteError(400, 'Invalid request');
    }

    if(!event.body){
        throw new NoteError(400, 'Invalid request body');
    }
}

const getConfig = () => {
    return {
        convertEmptyValues: true,
        ...(process.env.MOCK_DYNAMODB_ENDPOINT && {
            endpoint: process.env.MOCK_DYNAMODB_ENDPOINT,
            sslEnabled: false,
            region: "local",
          }),
    };

}