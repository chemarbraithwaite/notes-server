import { APIGatewayEvent, APIGatewayProxyResult } from "aws-lambda";
import { addNote } from "./add-note";
import { defaultHeaders, errorHandler } from "../_shared/errors";


export const handler = async (event: APIGatewayEvent): Promise<APIGatewayProxyResult> => {
    try {
        const note = await addNote(event);

        return{
            body: JSON.stringify(note),
            statusCode: 200,
            headers: defaultHeaders
        };

    } catch (error) {
        console.log(error);
        return errorHandler(error);
    }
    
}
