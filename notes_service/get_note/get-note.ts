import { APIGatewayEvent } from 'aws-lambda';
import { DynamoDB } from 'aws-sdk';
import { NoteError } from '../_shared/errors';

export const getNote = async (event: APIGatewayEvent) => {
    const tableName = process.env.TABLE_NAME;

    if (!tableName) {
        throw new NoteError(500, "Environment variable 'Table Name' is undefined")
    }

    validateEvent(event);

    try {
        const config = getConfig();
        const docClient = new DynamoDB.DocumentClient(config);
        const author = event.pathParameters!.author;
        const id = event.pathParameters!.id!;

        const params: DynamoDB.DocumentClient.GetItemInput = {
            TableName: tableName,
            Key: { author: author, id: id }
        };

        const response = await docClient.get(params).promise();

        if (!response.Item) {
            throw new NoteError(404, 'Note not found')
        }

        return response.Item;

    } catch (error) {
        console.log(error);

        if (error instanceof NoteError) {
            throw error;
        }

        throw new NoteError(500, 'Error getting note');
    }
}

const validateEvent = (event: APIGatewayEvent): void => {
    if (!event.pathParameters || !event.pathParameters.author) {
        throw new NoteError(400, 'Request missing author');
    }

    if (!event.pathParameters.id) {
        throw new NoteError(400, 'Request missing note id');
    }
}

const getConfig = () => {
    return {
        convertEmptyValues: true,
        ...(process.env.MOCK_DYNAMODB_ENDPOINT && {
            endpoint: process.env.MOCK_DYNAMODB_ENDPOINT,
            sslEnabled: false,
            region: "local",
        }),
    };

}