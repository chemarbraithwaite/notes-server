import { APIGatewayEvent } from 'aws-lambda';
import { DynamoDB } from 'aws-sdk';
import { NoteError } from '../_shared/errors';
const RESULT_SIZE = 10;

export const getNotes = async (event: APIGatewayEvent, id?: string) => {
    const tableName = process.env.TABLE_NAME;

    if (!tableName) {
        throw new NoteError(500, "Environment variable 'Table Name' is undefined")
    }

    if(!event.pathParameters || !event.pathParameters.author){
        throw new NoteError(400, 'Request missing author');
    }

    const author = event.pathParameters.author; 

    try {
        const config = getConfig();
        const docClient = new DynamoDB.DocumentClient(config);
        const params: DynamoDB.DocumentClient.QueryInput = {
            TableName: tableName,
            Limit: RESULT_SIZE,
            KeyConditionExpression: 'author = :author',
            ExpressionAttributeValues: {
                ':author': author
            },
            ScanIndexForward: false
        };

        if(id){
            params.ExclusiveStartKey = {author: author, id: id};
        }

        const response =  await docClient.query(params).promise();
        return {
            notes: response.Items,
            lastItem: response.LastEvaluatedKey
        };
    } catch (error) {
        console.log(error);
        throw new NoteError(500, 'Error getting notes');
    }
}

const getConfig = () => {
    return {
        convertEmptyValues: true,
        ...(process.env.MOCK_DYNAMODB_ENDPOINT && {
            endpoint: process.env.MOCK_DYNAMODB_ENDPOINT,
            sslEnabled: false,
            region: "local",
          }),
    };

}