import { APIGatewayEvent } from 'aws-lambda';
import { DynamoDB } from 'aws-sdk';
import { NoteError } from '../_shared/errors';

export class Note {
    id: string;
    author: string;
    title: string;
    content: string;
    color: string;

    constructor(note: any, author: string, id: string) {
        if (!note.title || !note.content || !note.color) {
            throw new NoteError(400, 'Invalid request body');
        }

        this.id = id;
        this.author = author;
        this.content = note.content;
        this.title = note.title;
        this.color = note.color;
    }

}

export const editNote = async (event: APIGatewayEvent) => {
    const tableName = process.env.TABLE_NAME;

    if (!tableName) {
        throw new NoteError(500, "Environment variable 'Table Name' is undefined");
    }

    validateEvent(event);

    const author = event.pathParameters?.author!;
    const id = event.pathParameters?.id!;
    const body = JSON.parse(event.body!);

    const note = new Note(body, author, id);

    const config = getConfig();

    try {
        const docClient = new DynamoDB.DocumentClient(config);

        const updateParams: DynamoDB.DocumentClient.Update = {
            TableName: tableName,
            Key: { author: note.author, id: note.id },
            ConditionExpression: 'author=:author AND id=:id',
            UpdateExpression: "set title=:title, content=:content, color=:color",
            ExpressionAttributeValues: {
                ":title": note.title,
                ":color": note.color,
                ":content": note.content,
                ":author": note.author,
                ":id": note.id
            }
        }

        await docClient.update(updateParams).promise();
    } catch (error) {

        console.log(error);
        throw new NoteError(500, 'Error updating note');
    }

    return note;
}

const validateEvent = (event: APIGatewayEvent): void => {

    if (!event.body) {
        throw new NoteError(400, 'Invalid request');
    }

    if (!event.pathParameters || !event.pathParameters.author) {
        throw new NoteError(400, 'Request missing author');
    }

    if (!event.pathParameters.id) {
        throw new NoteError(400, 'Request missing note id');
    }
}

const getConfig = () => {
    return {
        convertEmptyValues: true,
        ...(process.env.MOCK_DYNAMODB_ENDPOINT && {
            endpoint: process.env.MOCK_DYNAMODB_ENDPOINT,
            sslEnabled: false,
            region: "local",
        }),
    };

}