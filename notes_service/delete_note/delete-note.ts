import { APIGatewayEvent } from 'aws-lambda';
import { DynamoDB } from 'aws-sdk';
import { NoteError } from '../_shared/errors';

export const deleteNote = async (event: APIGatewayEvent) => {
    const tableName = process.env.TABLE_NAME;

    if (!tableName) {
        throw new NoteError(500, "Environment variable 'Table Name' is undefined")
    }

    validateRequest(event);

    const author = event.pathParameters!.author;
    const id = event.pathParameters!.id;

    try {
        const config = getConfig();
        const docClient = new DynamoDB.DocumentClient(config);
        const params: DynamoDB.DocumentClient.DeleteItemInput = {
            TableName: tableName,
            Key: { author: author, id: id }
        };

        const deleteOutput = await docClient.delete(params).promise();

        if (deleteOutput.$response.error) {
            console.log(deleteOutput.$response.error);
            throw new NoteError(500, 'Error deleting note')
        }

        return true;

    } catch (error) {

        if (error instanceof NoteError) {
            throw error;
        }

        console.log(error);
        throw new NoteError(500, 'Error deleting note');
    }
}

const validateRequest = (event: APIGatewayEvent) => {
    if (!event.pathParameters || !event.pathParameters.author) {
        throw new NoteError(400, 'Request missing author');
    }

    if (!event.pathParameters.id) {
        throw new NoteError(400, 'Request missing note id');
    }
}

const getConfig = () => {
    return {
        convertEmptyValues: true,
        ...(process.env.MOCK_DYNAMODB_ENDPOINT && {
            endpoint: process.env.MOCK_DYNAMODB_ENDPOINT,
            sslEnabled: false,
            region: "local",
        })
    };

}