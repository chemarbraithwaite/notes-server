[![pipeline status](https://gitlab.com/chemarbraithwaite/notes-server/badges/master/pipeline.svg)](https://gitlab.com/chemarbraithwaite/notes-server/-/commits/master)
[![coverage report](https://gitlab.com/chemarbraithwaite/notes-server/badges/master/coverage.svg)](https://gitlab.com/chemarbraithwaite/notes-server/-/commits/master)

# Notes Backend

This project uses [AWS CDK](https://aws.amazon.com/cdk/) to deploy a severless backend for the Note App (see the front end [here](https://gitlab.com/chemarbraithwaite/notes-ui)).

The CDK stack creates a RESTful API, using [Amazon API Gateway](https://aws.amazon.com/api-gateway/), that connects to AWS Lambda functions, written in NodeJs. The stack also creates a [Amazon Cognito](https://aws.amazon.com/cognito/) user pool that is used to authorize requests to the API.

The entire project, including the lambdas, is written in TypeScript.

## Configuration

The Cognito user pool requires a unique cognito domain prefix. Do the follow to configure this for your stack:

1. Create a file named config.ts in the `lib` folder
2. Copy and paste the following in the file, replacing `domain-prefix` with your unique domain prefix

```typescript
export const domain_prefix = 'domain-prefix';
```

## Useful commands

 * `npm run build`   compile typescript to js
 * `npm run watch`   watch for changes and compile
 * `npm run test`    perform the jest unit tests
 * `cdk deploy`      deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk synth`       emits the synthesized CloudFormation template
