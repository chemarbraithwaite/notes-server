import DynamoDB from "aws-sdk/clients/dynamodb";
import { editNote, Note } from "../../../notes_service/edit_note/edit-note";

const TABLE_NAME = 'Notes';
const title = 'My First Note';
const author = 'Test User';
const content = 'This is my first note';
const id = `${new Date(2019, 5, 3, 12, 34).toISOString()}id`;
const created = `${new Date(2019, 5, 3, 12, 34).toISOString()}`;
const color = '#000';




describe('Edit Note', () => {
    let event: any;

    const OLD_ENV = process.env;

    beforeEach(() => {
        event = {
            pathParameters: {
                author,
                id
            },
            body: JSON.stringify({
                content: content,
                title: title,
                color: color
            })
        };

        process.env = {
            ...OLD_ENV,
            TABLE_NAME: TABLE_NAME
        };

        jest.setTimeout(10000);

    });

    afterAll(() => {
        process.env = OLD_ENV;
    });

    it('Throws if table name is not set in environment variables', async () => {
        delete process.env.TABLE_NAME;
        await expect(editNote(event)).rejects.toThrow("Environment variable 'Table Name' is undefined");
    });

    it('Throws if user attribute is not valid (contains title and content), ', async () => {
        event.body = JSON.stringify(
            {
                content: content
            }
        );

        await expect(editNote(event)).rejects.toThrow('Invalid request body');
    });

    it('Throws if author is missing from path paramters', async () => {
        delete event.pathParameters.author;
        await expect(editNote(event)).rejects.toThrow('Request missing author');
    });

    it('Throws if id is missing from path paramters', async () => {
        delete event.pathParameters.id;
        await expect(editNote(event)).rejects.toThrow('Request missing note id');
    });

    it('Throws if event body is undefined', async () => {
        delete event.body;
        await expect(editNote(event)).rejects.toThrow('Invalid request');
    });

    it('Throws if note does not exist', async () => {
        await expect(editNote(event)).rejects.toThrow('Error updating note');
    });

    it('Note is edited in db', async () => {

        const config = {
            convertEmptyValues: true,
            ...(process.env.MOCK_DYNAMODB_ENDPOINT && {
                endpoint: process.env.MOCK_DYNAMODB_ENDPOINT,
                sslEnabled: false,
                region: "local",
            }),
        };

        const docClient = new DynamoDB.DocumentClient(config);

        const item: DynamoDB.DocumentClient.PutItemInput = {
            TableName: TABLE_NAME,
            Item: {
                content: 'Content before edit',
                title: 'Title before edit',
                color: 'Color before edit',
                created,
                id,
                author
            }
        }

        await docClient.put(item).promise();

        await editNote(event);

        const params: DynamoDB.DocumentClient.GetItemInput = {
            TableName: TABLE_NAME,
            Key: { author: author, id: id }
        };

        const response = await docClient.get(params).promise();

        expect(response.Item).toEqual({
            ...JSON.parse(event.body),
            created,
            author,
            id
        });

    });

    it('Edited note is returned', async () => {
        
        const config = {
            convertEmptyValues: true,
            ...(process.env.MOCK_DYNAMODB_ENDPOINT && {
                endpoint: process.env.MOCK_DYNAMODB_ENDPOINT,
                sslEnabled: false,
                region: "local",
            }),
        };

        const docClient = new DynamoDB.DocumentClient(config);

        const item: DynamoDB.DocumentClient.PutItemInput = {
            TableName: TABLE_NAME,
            Item: {
                content: 'Content before edit',
                title: 'Title before edit',
                color: 'Color before edit',
                created,
                id,
                author
            }
        }

        await docClient.put(item).promise();

        const returnedNote = await editNote(event);

        expect(returnedNote).toEqual({
            ...JSON.parse(event.body),
            author,
            id
        });
    });

});


describe('Note', () => {

    let noteObject: any;

    beforeEach(() => {
        noteObject = {
            content: content,
            title: title,
            color: color
        };
    });


    it('Throws if note object does not have content', () => {
        delete noteObject.content;
        expect(() => new Note(noteObject, author, id)).toThrow();
    });

    it('Throws if note object does not have title', () => {
        delete noteObject.title;
        expect(() => new Note(noteObject, author, id)).toThrow();
    });

    it('Throws if note object does not have color', () => {
        delete noteObject.color;
        expect(() => new Note(noteObject, author, id)).toThrow();
    });

    it('Note is initialized properly', () => {
        const note = new Note(noteObject, author, id);

        expect(note.title).toBe(title);
        expect(note.author).toBe(author);
        expect(note.content).toBe(content);
        expect(note.id).toBe(id);
        expect(note.color).toBe(color);
    });
});