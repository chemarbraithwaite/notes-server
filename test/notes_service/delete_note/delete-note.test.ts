import { APIGatewayProxyEventQueryStringParameters } from "aws-lambda";
import DynamoDB, { DocumentClient } from "aws-sdk/clients/dynamodb";
import { deleteNote } from "../../../notes_service/delete_note/delete-note";
const TABLE_NAME = 'Notes';
const author = 'Chemar';
const id = `${new Date(2019, 2, 2, 12).toISOString()}id`;

describe('Get Notes', () => {
    const OLD_ENV = process.env;
    let event: any;

    beforeEach(() => {
        event = {
            pathParameters: {
                author,
                id
            }
        }

        process.env = {
            ...OLD_ENV,
            TABLE_NAME: TABLE_NAME
        };
    });

    afterAll(() => {
        process.env = OLD_ENV;
    });

    it('Throws if table name is not set in environment variables', async () => {
        delete process.env.TABLE_NAME;
        await expect(deleteNote(event)).rejects.toThrow("Environment variable 'Table Name' is undefined");
    });

    it('Throws if author is missing from path parameter string', async () => {
        delete event.pathParameters.author;
        await expect(deleteNote(event)).rejects.toThrow("Request missing author");
    });

    it('Throws if id is missing from path parameter string', async () => {
        delete event.pathParameters.id;
        await expect(deleteNote(event)).rejects.toThrow("Request missing note id");
    });

    it('Returns true on delete', async () => {
        const response = await deleteNote(event);
        expect(response).toBe(true);
    });

    it('Deletes note if note is in db', async () => {

        const config = {
            convertEmptyValues: true,
            ...(process.env.MOCK_DYNAMODB_ENDPOINT && {
                endpoint: process.env.MOCK_DYNAMODB_ENDPOINT,
                sslEnabled: false,
                region: "local",
            }),
        };

        const docClient = new DynamoDB.DocumentClient(config);
        const note = {
            author: author,
            content: `Note #1's content`,
            id: id,
            title: `Note #1`
        };

        const user: DynamoDB.DocumentClient.PutItemInput = {
            TableName: TABLE_NAME,
            Item: {
                ...note
            }
        }

        await docClient.put(user).promise();
        await deleteNote(event);

        const getItemInput: DynamoDB.DocumentClient.GetItemInput = {
            TableName: TABLE_NAME,
            Key: { author: author, id: id }
        };

        const getResponse = await docClient.get(getItemInput).promise();

        expect(getResponse.Item).toBeUndefined();
    });

});