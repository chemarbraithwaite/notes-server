import DynamoDB from "aws-sdk/clients/dynamodb";
import { getNote } from "../../../notes_service/get_note/get-note";
const TABLE_NAME = 'Notes';
const author = 'Chemar';
const id = `${new Date(2019, 2, 2, 12).toISOString()}id`;

describe('Get Note', () => {
    const OLD_ENV = process.env;
    let event: any;

    beforeEach(() => {
        event = {
            pathParameters: {
                author,
                id
            }
        };

        process.env = {
            ...OLD_ENV,
            TABLE_NAME: TABLE_NAME
        };
    });

    afterAll(() => {
        process.env = OLD_ENV;
    });

    it('Throws if table name is not set in environment variables', async () => {
        delete process.env.TABLE_NAME;
        await expect(getNote(event)).rejects.toThrow("Environment variable 'Table Name' is undefined");
    });

    it('Throws if author is missing from path parameter', async () => {
        delete event.pathParameters.author;
        await expect(getNote(event)).rejects.toThrow("Request missing author");
    });

    it('Throws if id is missing from path parameter', async () => {
        delete event.pathParameters.id;
        await expect(getNote(event)).rejects.toThrow("Request missing note id");
    });

    it('Throws not found if note is not found', async () => {
        await expect(getNote(event)).rejects.toThrow("Note not found");
    });

    it('Returns note if note is in db', async () => {

        const config = {
            convertEmptyValues: true,
            ...(process.env.MOCK_DYNAMODB_ENDPOINT && {
                endpoint: process.env.MOCK_DYNAMODB_ENDPOINT,
                sslEnabled: false,
                region: "local",
            }),
        };

        const docClient = new DynamoDB.DocumentClient(config);
        const note = {
            author,
            content: `Note #1's content`,
            id,
            title: `Note #1`
        };

        const user: DynamoDB.DocumentClient.PutItemInput = {
            TableName: TABLE_NAME,
            Item: {
                ...note
            }
        }

        await docClient.put(user).promise();
        const retrievedNote = await getNote(event);

        expect(retrievedNote).toEqual(note);
    });

});