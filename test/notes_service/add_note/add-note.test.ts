import { DocumentClient } from "aws-sdk/clients/dynamodb";
import { addNote, Note } from "../../../notes_service/add_note/add-note";

const TABLE_NAME = 'Notes';
const title = 'My First Note';
const author = 'Test User';
const content = 'This is my first note';
const created = new Date(2019, 5, 3, 12, 34).toISOString();
const id = `${created}id`;
const color = '#000';



describe('Add Note To Database', () => {
    let event: any;

    const OLD_ENV = process.env;

    beforeEach(() => {
        event = {
            pathParameters: {
                author: author
            },
            body: JSON.stringify({
                author: author,
                content: content,
                title: title,
                color: color
            })
        };

        process.env = {
            ...OLD_ENV,
            TABLE_NAME: TABLE_NAME,
            CREATED: created.toString(),
            NOTE_ID: id
        };

    });

    afterAll(() => {
        process.env = OLD_ENV;
    });

    it('Throws if table name is not set in environment variables', async () => {
        delete process.env.TABLE_NAME;
        await expect(addNote(event)).rejects.toThrow("Environment variable 'Table Name' is undefined");
    });

    it('Throws if request body is not valid (contains title and content), ', async () => {
        event.body = JSON.stringify(
            {
                content: content
            }
        );

        await expect(addNote(event)).rejects.toThrow('Invalid request body');
    });

    it('Throws if event does not have a body', async () => {
        delete event.body;
        await expect(addNote(event)).rejects.toThrow('Invalid request body');
    });

    it('Throws if path parameters does not have author', async () => {
        delete event.pathParameters.author;
        await expect(addNote(event)).rejects.toThrow('Invalid request');
    });

    it('Successfully user adds to database', async () => {
        await addNote(event);
        const config = {
            convertEmptyValues: true,
            ...(process.env.MOCK_DYNAMODB_ENDPOINT && {
                endpoint: process.env.MOCK_DYNAMODB_ENDPOINT,
                sslEnabled: false,
                region: "local",
            }),
        };

        const docClient = new DocumentClient(config);

        const { Item } = await docClient.get({ TableName: TABLE_NAME, Key: { author: author, id: id } }).promise();

        expect(Item).toEqual({
            ...JSON.parse(event.body),
            created: Item?.created,
            author,
            id
        });
    });

    it('Returns note created', async () => {
        const returnedNote = await addNote(event);

        expect(returnedNote).toEqual({
            ...JSON.parse(event.body),
            created: returnedNote.created,
            author,
            id
        });
    });

});


describe('Note', () => {

    let noteObject: any;

    beforeEach(() => {
        noteObject = {
            content: content,
            title: title,
            color: color
        };
    });

    it('Throws if note object does not have author', () => {
        expect(() => new Note(noteObject, undefined as any)).toThrow();
    });

    it('Throws if note object does not have content', () => {
        delete noteObject.content;
        expect(() => new Note(noteObject, author)).toThrow();
    });

    it('Throws if note object does not have color', () => {
        delete noteObject.color;
        expect(() => new Note(noteObject, author)).toThrow();
    });

    it('Throws if note object does not have title', () => {
        delete noteObject.title;
        expect(() => new Note(noteObject, author)).toThrow();
    });

    it('Note is initialized properly', () => {
        const note = new Note(noteObject, author);

        expect(note.title).toBe(title);
        expect(note.author).toBe(author);
        expect(note.content).toBe(content);
        expect(note.color).toBe(color);
    });
});