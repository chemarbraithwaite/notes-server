import { DocumentClient } from "aws-sdk/clients/dynamodb";
import { getNotes } from "../../../notes_service/get_notes/get-notes";
const TABLE_NAME = 'Notes';
const author = 'Chemar';

describe('Get Notes', () => {
    const OLD_ENV = process.env;
    let event: any;

    beforeEach(() => {
        event = {
            pathParameters: {
                author
            }
        }

        process.env = {
            ...OLD_ENV,
            TABLE_NAME: TABLE_NAME
        };
    });

    afterAll(() => {
        process.env = OLD_ENV;
    });

    it('Throws if table name is not set in environment variables', async () => {
        delete process.env.TABLE_NAME;
        await expect(getNotes(event)).rejects.toThrow("Environment variable 'Table Name' is undefined");
    });

    it('Throws if event is missing author in path parameter', async () => {
        delete event.pathParameters.author;
        await expect(getNotes(event)).rejects.toThrow('Request missing author');
    });

    it('Returns last 10 notes when no last item is provided', async () => {
        await seedDatabase();
        const returnedNotes = await getNotes(event);
        const expectedNotes = generateNotes(23, 14);

        expect(returnedNotes).toEqual({
            notes: expectedNotes,
            lastItem: {
                author: expectedNotes[expectedNotes.length - 1].author,
                id: expectedNotes[expectedNotes.length - 1].id
            }
        });

    });

    it('Return next 10 items after start key when start key is provided', async () => {
        const lastNote = getNote(14);
        await seedDatabase();

        const returnedNotes = await getNotes(event, lastNote.id);
        const expectedNotes = generateNotes(13, 4);
        expect(returnedNotes).toEqual({
            notes: expectedNotes,
            lastItem: {
                author: expectedNotes[expectedNotes.length - 1].author,
                id: expectedNotes[expectedNotes.length - 1].id
            }
        });
    });

});

const seedDatabase = async () => {
    let notes: { PutRequest: { Item: { author: string; id: string, content: string; created: string; title: string; }; }; }[] = [];

    const config = {
        convertEmptyValues: true,
        ...(process.env.MOCK_DYNAMODB_ENDPOINT && {
            endpoint: process.env.MOCK_DYNAMODB_ENDPOINT,
            sslEnabled: false,
            region: "local",
        }),
    };

    for (let index = 0; index < 24; index++) {
        notes.push({
            PutRequest: {
                Item: {
                    author: author,
                    content: `Note #${index}'s content`,
                    created: getDate(index),
                    id: `${getDate(index)}id`,
                    title: `Note #${index}`
                }
            }
        });
    }

    const docClient = new DocumentClient(config);
    const batchWriteResponse = await docClient.batchWrite({
        RequestItems: {
            'Notes': notes
        }
    }).promise();

    if (batchWriteResponse.$response.error) {
        fail(batchWriteResponse.$response.error);
    }
}

const getNote = (noteId: number) => {
    return {
        author: author,
        content: `Note #${noteId}'s content`,
        created: getDate(noteId),
        title: `Note #${noteId}`,
        id: `${getDate(noteId)}id`
    };
}

const generateNotes = (largest: number, smallest = 0) => {


    if (largest < smallest) {
        fail('Notes generated incorrectly, largest note is smaller than smallest');
    }

    const generatedNotes = [];

    for (let index = largest; index >= smallest; index--) {
        generatedNotes.push(
            getNote(index)
        );
    }

    return generatedNotes;
}

const getDate = (offset: number) => {
    return new Date(2019, 4, 21, 12, offset).toISOString();
}